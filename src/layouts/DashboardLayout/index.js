// import React from 'react';
// import { Outlet } from 'react-router-dom';
// import { makeStyles } from '@material-ui/core';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: theme.palette.background.dark,
//     display: 'flex',
//     height: '100%',
//     overflow: 'hidden',
//     width: '100%'
//   },
//   wrapper: {
//     display: 'flex',
//     flex: '1 1 auto',
//     overflow: 'hidden',
//     paddingTop: 64,
//     // [theme.breakpoints.up('lg')]: {
//     //   paddingLeft: 256
//     // }
//   },
//   contentContainer: {
//     display: 'flex',
//     flex: '1 1 auto',
//     overflow: 'hidden'
//   },
//   content: {
//     flex: '1 1 auto',
//     height: '100%',
//     overflow: 'auto'
//   }
// }));

// const DashboardLayout = () => {
//   const classes = useStyles();

//   return (
//     <div className={classes.root}>
//       <div className={classes.wrapper}>
//         <div className={classes.contentContainer}>
//           <div className={classes.content}>
//             <Outlet />
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default DashboardLayout;


import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { fade, makeStyles } from '@material-ui/core/styles';
import { Outlet } from 'react-router-dom';
import {
  Avatar,
  CircularProgress 
} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

import PowerIcon from '@material-ui/icons/Power';
import PowerOffIcon from '@material-ui/icons/PowerOff';
import RefreshIcon from '@material-ui/icons/Refresh';

import { store } from 'src/AppStore/store'
import {  REFRESH } from 'src/AppStore/actions'

import MQTT from 'paho-mqtt'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
    fontSize: "22px",
    paddingLeft: "20px"
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
    avatar: {
      paddingRight: "20px"
    }
  },
}));

export default function SearchAppBar(props) {
  const classes = useStyles();
  
  const [client] = React.useState(props.client)
  const [Device, setDevice] = React.useState('')
  const [srefrash, setsrefrash] = React.useState(false)
  const [isConnect, setisConnect] = React.useState(false)
  const intervalcheck = () => {
    setInterval(function(){
      setsrefrash(store.getState().isrefresh)
      setisConnect(store.getState().isConnect)
      setDevice(store.getState().device)
    }, 100);
  }

  intervalcheck()

  return (
    <div className={classes.root}>
      <AppBar position="static" style = {{backgroundColor: "#074c93"}}>
        <Toolbar>
          <Avatar
            className={classes.avatar}
            component={RouterLink}
            src={'/static/logo.png'}
            to="/app/Config"
          />
          <Typography className={classes.title} variant="h6" noWrap>
            PWA Logger Config
          </Typography>
          <div>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
              onClick={() => {
                // window.location.reload()
                console.log(store.getState().isrefresh)
                setsrefrash(!srefrash)
                if (!store.getState().isrefresh){
                  store.dispatch(REFRESH(true))
                  var message = new MQTT.Message("get config");
                  message.destinationName = "smartlogger/"+Device+"/modbus/config/get"
                  client.send(message);
                  message = new MQTT.Message("get config");
                  message.destinationName = "smartlogger/"+Device+"/device/config/get"
                  client.send(message);
                }
              }}
            >
              {
                ((srefrash) ? <CircularProgress color="inherit" />: <RefreshIcon fontSize="large" />)
              }
            </IconButton>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
              onClick={() => {
                window.location.reload()
              }}
            >
              {
                ((isConnect)? <PowerIcon fontSize="large" /> : <PowerOffIcon fontSize="large" />)
              }
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {/* components */}
      <Outlet />
    </div>
  );
}
