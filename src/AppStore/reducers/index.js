import initialState from './state'

export default (state = initialState, action) => {
  switch (action.type) {
    case 'STORESTOCK':
      return Object.assign({}, state, {
        Stock: action.payload
      })
    case 'STOREPROVICE':
      return Object.assign({}, state, {
        Provice: action.payload
      })
    case 'RESULTSEARCHSTOCK':
      return Object.assign({}, state, {
        ResultSearchStock: action.payload
      })
    case 'EXPORT':
      return Object.assign({}, state, {
        Export: action.payload
      })
    case 'RS485':
      return Object.assign({}, state, {
        RS485: action.payload
      })
    case 'CONFIG': 
      return Object.assign({}, state, {
        Config: action.payload
      })
    case "MODEL" :
      return Object.assign({}, state, {
        Model: action.payload
      })
    case "SAVE":
      return Object.assign({}, state, {
        save: action.payload
      })
    case "DEVICE":
      return Object.assign({}, state, {
        device: action.payload
      })
    case "REFRESH":
      return Object.assign({}, state, {
        isrefresh: action.payload
      })
    case "ISCONNECT": 
      return Object.assign({}, state, {
        isConnect: action.payload
      })
    default:
      return state
  }
}
