export const STORESTOCK = (payload) => {
  return {
    type : "STORESTOCK",
    payload
  }
}

export const STOREPROVICE = (payload) => {
  return {
    type: "STOREPROVICE",
    payload
  }
}

export const RESULTSEARCHSTOCK = (payload) => {
  return {
    type: "RESULTSEARCHSTOCK",
    payload
  }
}

export const EXPORT = (payload) => {
  return {
    type: "EXPORT",
    payload
  }
}

export const RS485 = (payload) => {
  return {
    type: "RS485",
    payload
  }
}

export const CONFIG = (payload) => {
  return {
    type: 'CONFIG',
    payload
  }
}

export const MODEL = (payload) => {
  return {
    type: "MODEL",
    payload
  }
}

export const SAVE = (payload) => {
  return {
    type: "SAVE",
    payload
  }
}

export const DEVICE = (payload) => {
  return {
    type: "DEVICE",
    payload
  }
}

export const REFRESH = (payload) => {
  return {
    type: "REFRESH",
    payload
  }
}

export const ISCONNECT = (payload) => {
  return {
    type: 'ISCONNECT',
    payload
  }
}