
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
}));

const PRVConfig = ({ className, store,  ...prop}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)}>
      
    </div>
  );
};

PRVConfig.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default PRVConfig;