import React from 'react';
import {
  Container,
  Grid,
  makeStyles,
} from '@material-ui/core';
import Page from 'src/components/Page';
import Contrainer from './Contrainer'

import { store } from 'src/AppStore/store'
import { MODEL } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  titleDialog:{
    fontSize: "24px",
    margin: "10px 0px 10px 10px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  }
}));

const Config = (props) => {
  const classes = useStyles();
  const [client] = React.useState(props.client)
  React.useEffect(() => {
    fetch('RS485.json').then(res => res.json())
    .then(
      (result) => {
        console.log(result)
        store.dispatch(MODEL(result))
      },
      (error) => {
        store.dispatch(MODEL(error))
      }
    )
  })

  return (
    <Page
      className={classes.root}
      title="PWA Logger Config"
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        ><Grid
            item
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
            <Contrainer client={client} store={store} />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default Config;


// topic detail
// smartlogger/:id/device/config/get
// smartlogger/:id/modbus/config/get
// smartlogger/:id/prvctrl/config/get