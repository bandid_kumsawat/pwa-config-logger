
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  TextField,
  Button
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  groupBTN: {
    margin: theme.spacing(1),
    width: '100%',
  }
}));

const SDCard = ({ className, store,  ...rest}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>SD Card</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >
              {/* // Date:  */}
              <Grid container spacing={1} >
                <Grid  item lg={12} md={12} xl={12} xs={12} >
                  <form className={classes.Inputbox} noValidate autoComplete="off">
                    <TextField
                      id="outlined-multiline-flexible"
                      label="SD Card"
                      multiline
                      rows={27}
                      variant="outlined"
                    />
                  </form>
                </Grid>
              </Grid>

              {/* // Date:  */}
              <Grid container spacing={1} className={classes.groupBTN}>
                <Grid  item lg={4} md={4} xl={4} xs={4} >
                  <Button variant="contained" color="primary" style = {{width: "100%"}}>
                    SD INFO
                  </Button>
                </Grid>
                <Grid  item lg={4} md={4} xl={4} xs={4} >
                  <Button variant="contained" color="primary" style = {{width: "100%"}}>
                    LISTFILE
                  </Button>
                </Grid>
                <Grid  item lg={4} md={4} xl={4} xs={4} >
                  <Button variant="contained" color="primary" style = {{width: "100%"}}>
                    CLEAR
                  </Button>
                </Grid>
              </Grid>


            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

SDCard.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default SDCard;