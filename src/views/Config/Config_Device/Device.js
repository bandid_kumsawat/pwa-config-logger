
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  TextField,
} from '@material-ui/core';
import { CONFIG } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  device:{
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#074c93",
    fontWeight: "700",
    marginLeft: "20px"
  }
}));

const Device = ({ className, store,  ...prop}) => {
  const classes = useStyles();
  const [config, setconfig] = React.useState([])

  React.useEffect(() => {
    const setcon = () => {
      setconfig(store.getState().Config)
    }
    setcon()
  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>Device</legend>
        <div>
          <Container maxWidth={false}>
            {/* // device name */}
            <Grid container spacing={1} >
              <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                Device Name:
              </Grid>
              <Grid  item lg={8} md={8} xl={8} xs={12} >
                <form className={classes.Inputbox + " " + classes.device} noValidate autoComplete="off">
                  {((config.length > 0) ? config[0].dev: "กรุณาเลือกอุปกรณ์")}
                </form>
              </Grid>
            </Grid>

            {/* // Sampling Rate */}
            <Grid container spacing={1} >
              <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                Sampling Rate:
              </Grid>
              <Grid  item lg={7} md={7} xl={7} xs={12} >
                <form className={classes.Inputbox} noValidate autoComplete="off">
                  <TextField id="outlined-basic" 
                  type={'number'}
                  onChange={
                    (e) => {
                      var temp = config[0]
                      temp.sr = e.target.value
                      temp.sr = Number(temp.sr)
                      setconfig([temp])
                      store.dispatch(CONFIG([temp]))
                    }
                  }
                  value={
                    ((config.length > 0) ? config[0].sr: "")
                  } 
                  label="Sampling Rate" variant="outlined" disabled={!(config.length > 0)} />
                </form>
              </Grid>
              <Grid  item lg={1} md={1} xl={1} xs={1} className={classes.label}>
                Sec
              </Grid>
            </Grid>

            {/* // Upload */}
            <Grid container spacing={1} >
              <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                Upload:
              </Grid>
              <Grid  item lg={7} md={7} xl={7} xs={7} >
                <form className={classes.Inputbox} noValidate autoComplete="off">
                  <TextField id="outlined-basic" 
                  type={'number'}
                  onChange={
                    (e) => {
                      var temp = config[0]
                      temp.tr = e.target.value
                      temp.tr = Number(temp.tr)
                      setconfig([temp])
                      store.dispatch(CONFIG([temp]))
                    }
                  }
                  value={
                    ((config.length > 0) ? config[0].tr: "")
                  }
                  label="Upload" variant="outlined" disabled={!(store.getState().Config.length)} />
                </form>
              </Grid>
              <Grid  item lg={1} md={1} xl={1} xs={1} className={classes.label}>
                Sec
              </Grid>
            </Grid>

            {/* // เปลี่ยนโหมดแรงดัน */}
            <Grid container spacing={1} >
              <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                เปลี่ยนโหมดแรงดัน:
              </Grid>
              <Grid  item lg={7} md={7} xl={7} xs={7} >
                <form className={classes.Inputbox} noValidate autoComplete="off">
                  <TextField id="outlined-basic" 
                  type={'number'}
                  onChange={
                    (e) => {
                      var temp = config[0]
                      temp.bsf = e.target.value
                      temp.bsf = Number(temp.bsf)
                      setconfig([temp])
                      store.dispatch(CONFIG([temp]))
                    }
                  }
                  value={
                    ((config.length > 0) ? config[0].bsf: "")
                  }
                  label="เปลี่ยนโหมดแรงดัน" variant="outlined" disabled={false} />
                </form>
              </Grid>
              <Grid  item lg={1} md={1} xl={1} xs={1} className={classes.label}>
                V
              </Grid>
            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

Device.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default Device;