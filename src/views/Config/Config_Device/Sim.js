
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  // TextField,
} from '@material-ui/core';
// import { CONFIG } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  apn: {
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#000000",
    fontWeight: "400",
    marginLeft: "20px"
  }
}));

const Sim = ({ className, store,  ...rest}) => {
  const classes = useStyles();
  const [config, setconfig] = React.useState([])

  React.useEffect(() => {
    const setcon = () => {
      setconfig(store.getState().Config)
    }
    setcon()
  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>SIM</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >

              {/* // APN:  */}
              <Grid container spacing={1} >
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                  APN: 
                </Grid>
                <Grid  item lg={8} md={8} xl={8} xs={8} >
                  <form className={classes.Inputbox + " " + classes.apn} noValidate autoComplete="off">
                    {((config.length > 0) ? config[0].apn: "-")}
                    {/* <TextField id="outlined-basic5" 
                    onChange={
                      (e) => {
                        var temp = config[0]
                        temp.apn = e.target.value
                        setconfig([temp])
                        store.dispatch(CONFIG([temp]))
                      }
                    }
                    value={
                      ((config.length > 0) ? config[0].apn: "")
                    } 
                    label="APN" variant="outlined" disabled={!(config.length > 0)} /> */}
                  </form>
                </Grid>
              </Grid>
              
            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

Sim.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default Sim;