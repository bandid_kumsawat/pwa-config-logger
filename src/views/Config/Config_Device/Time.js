
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  Button
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  datetime: {
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#074c93",
    fontWeight: "700",
    marginLeft: "20px"
  }
}));

const Time = ({ className, store,  ...rest}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>Logger Time</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >
              {/* // Date:  */}
              <Grid container spacing={1} >
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                  Date: 
                </Grid>
                <Grid  item lg={8} md={8} xl={8} xs={8} >
                  <form className={classes.Inputbox + " " + classes.datetime} noValidate autoComplete="off" >
                    {"02/02/1970"}
                  </form>
                </Grid>
              </Grid>

              {/* // Time:  */}
              <Grid container spacing={1} >
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                  Time: 
                </Grid>
                <Grid  item lg={8} md={8} xl={8} xs={8} >
                  <form className={classes.Inputbox + " " + classes.datetime} noValidate autoComplete="off" >
                    {"17:18:23"}
                  </form>
                </Grid>
              </Grid>

              {/* // Button:  */}
              <Grid container spacing={1} >
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}></Grid>
                <Grid  item lg={8} md={8} xl={8} xs={8} >
                  <form className={classes.Inputbox} noValidate autoComplete="off">
                    <Button variant="contained" color="primary">
                      Sync Time to Board
                    </Button>
                  </form>
                </Grid>
              </Grid>

            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

Time.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default Time;