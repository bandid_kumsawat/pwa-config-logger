
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  FirmwareVersion :{
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#000000",
    fontWeight: "400",
  },
  version:{
    marginLeft: "20px"
  }
}));

const Firmware = ({ className, store,  ...rest}) => {
  const classes = useStyles();
  const [config, setconfig] = React.useState([])

  React.useEffect(() => {
    const setcon = () => {
      setconfig(store.getState().Config)
    }
    setcon()
  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>Firmware Version</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >
              {/* // Script */}
              <Grid container spacing={1} style = {{padding: "0px 0px 20px 0px"}}>
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                  Version: 
                </Grid>
                <Grid  item lg={8} md={8} xl={8} xs={8} className={classes.FirmwareVersion}>
                  <form className={classes.Inputbox + " " + classes.version} noValidate autoComplete="off" >
                    {((config.length > 0) ? config[0].ver: "กรุณาเลือกอุปกรณ์")}
                  </form>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

Firmware.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default Firmware;