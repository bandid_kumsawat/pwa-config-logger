
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  Button,
} from '@material-ui/core';
import { RS485 } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  RS485Config:{
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#074c93",
    fontWeight: "700",
    marginLeft: "20px"
  },
  rs485col1: {
    textAlign: "right",
    marginTop: "20px",
    fontSize: "20px"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: '100%',
  },
  TextField: {
    marginLeft: "7px"
  }
}));

const RS485Config = ({ className, store,  ...prop}) => {
  const classes = useStyles();
  const [config, setconfig] = React.useState([])
  const [model, setmodel] = React.useState([])
  const [serialparam] = React.useState({
    baudrate: [
      1200,
      2400,
      4800,
      9600,
      14400,
      19200,
      38400,
      57600,
      115200,
      230400,
      460800,
    ],
    databit: [
      7,8
    ],
    parity: [
      "Even",
      "Old",
      "None"
    ],
    stopbit: [
      "1", "2"
    ]
  })
  const [select_model, setselect_model] = React.useState('')
  const [select_boardrate, setselect_boardrate] = React.useState('')
  const [select_parity, setselect_parity] = React.useState('')
  const [select_stopbit, setselect_stopbit] = React.useState('')
  const [rs485, setrs485] = React.useState([])

  React.useEffect(() => {
    const setcon = () => {
      if (store.getState().RS485.length > 0){
        // check no found model
        var model = store.getState().Model
        var rs485_ = store.getState().RS485
        var found = false
        if (rs485_[0].mml[0] !== undefined){
          model.forEach((item ,index) => {
            if (rs485_[0].mml[0].mn === item.mn){
              found = true
            }
          })
          if (found === false){
            model.push({
              "mn": rs485_[0].mml[0].mn,
              "mc": 1,
              "reg": [
                  {
                      "mid": 1,
                      "mbf": 3,
                      "mba": rs485_[0].mrs[0].mba,
                      "tag": "fl1"
                  },
                  {
                      "mid": 1,
                      "mbf": 3,
                      "mba": rs485_[0].mrs[1].mba,
                      "tag": "fa1"
                  }
              ]
            })
          }
        }else {

        }

        setconfig(store.getState().Config)
        setmodel(store.getState().Model)
        setrs485(store.getState().RS485)
        var data = store.getState().RS485[0]
        if (data.mml.length > 0){
          setselect_model(data.mml[0].mn)
        }else {
          setselect_model("None")
        }
        setselect_boardrate(data.mbd)
  
        var mpr = ''
        if (data.mpr === 0){
          mpr = 'None'
        }else if (data.mpr === 1){
          mpr = 'Old'
        }else if (data.mpr === 2){
          mpr = "Even"
        }else {
          mpr = ""
        }
  
        setselect_parity(mpr)
        setselect_stopbit(data.mst)
      }
    }
    setcon()
  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>RS485Config</legend>
        <div>
          <Container maxWidth={false}>
            <Grid container spacing={1} >
              <Grid item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Baudrate:</div>
              </Grid>
              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <FormControl variant="outlined" className={classes.formControl} style={{width:"100%"}} disabled={!(store.getState().RS485.length > 0)}>
                  <InputLabel htmlFor="outlined-age-native-simple">Baudrate</InputLabel>
                  <Select
                    native
                    label="Baudrate:"
                    inputProps={{
                      name: 'Baudrate:',
                      id: 'outlined-age-native-simple',
                    }}
                    onChange={(e) => {
                      setselect_model(e.target.value)
                      var temp = store.getState().RS485 //= e.target.value
                      temp[0].mbd = Number(e.target.value)
                      store.dispatch(RS485(temp))
                    }}
                    value={select_boardrate}
                  >
                    {
                      serialparam.baudrate.map((item ,index) => {
                        return (
                          <option value={item} key={index}>{item}</option>
                        )
                      })
                    }
                  </Select>
                </FormControl>
              </Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Meter Model:</div>
              </Grid>

              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <FormControl variant="outlined" className={classes.formControl} style={{width:"100%"}} disabled={!(store.getState().RS485.length > 0)}>
                  <InputLabel htmlFor="outlined-age-native-simple">Meter Model</InputLabel>
                  <Select
                    native
                    label="Meter Model:"
                    inputProps={{
                      name: 'Meter Model:',
                      id: 'outlined-age-native-simple',
                    }}
                    onChange={(e) => {
                      setselect_model(e.target.value)
                      var selected = e.target.value
                      var Modelrs458 = store.getState().Model
                      var select_medel = []
                      Modelrs458.forEach((item) => {
                        if (item.mn === selected){
                          select_medel.push(item)
                        }
                      })
                      // console.log(select_medel)
                      var temp = store.getState().RS485 //= e.target.value
                      if (temp[0].mml.length === 0){
                        temp[0].mml.push({
                          "mc": select_medel[0].mc,
                          "mn": select_medel[0].mn
                        })
                        temp[0].mrs.push(select_medel[0].reg[0])
                        temp[0].mrs.push(select_medel[0].reg[1])
                        console.log(temp)
                      }else{
                        temp[0].mml[0].mn = e.target.value
                        temp[0].mrs[0].mba = select_medel[0].reg[0].mba
                        temp[0].mrs[1].mba = select_medel[0].reg[1].mba
                        console.log(select_medel)
                        // temp[0].mrs[0].mba = temp[0].reg[0].mba
                        // temp[0].mrs[1].mba = temp[0].reg[1].mba
                      }
                      store.dispatch(RS485(temp))
                    }}
                    value={select_model}
                  >
                    {
                      ((store.getState().RS485[0].mml[0] === undefined) ? <option value={null} >{""}</option>: "")
                    }
                    {
                      model.map((item ,index) => {
                        var check = false
                        item.reg.forEach((item ,index) => {
                          if (item.tag === 'fa1' || item.tag === 'fl1'){
                            check = true
                          }
                        })
                        if (check){
                          return (
                            <option value={item.mn} key={index}>{item.mn}</option>
                          )
                        }
                        return 0
                      })
                    }
                  </Select>
                </FormControl>
              </Grid>
              
              
              <Grid  item lg={2} md={2} xl={2} xs={2}></Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Data bits:</div>
              </Grid>
              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <FormControl variant="outlined" className={classes.formControl} style={{width:"100%"}} disabled={!(store.getState().RS485.length > 0)}>
                  <InputLabel htmlFor="outlined-age-native-simple">Data bits</InputLabel>
                  <Select
                    native
                    label="Data bits:"
                    inputProps={{
                      name: 'Data bits:',
                      id: 'outlined-age-native-simple',
                    }}
                    onChange={
                      (e) => {
                        var temp = rs485[0]
                        temp.mbt = e.target.value
                        temp.mbt = Number(temp.mbt)
                        setrs485([temp])
                        store.dispatch(RS485([temp]))
                      }
                    }
                    value={
                      ((rs485.length > 0) ? rs485[0].mbt: "")
                    } 
                  >
                    {
                      serialparam.databit.map((item ,index) => {
                        return (
                          <option value={item} key={index}>{item}</option>
                        )
                      })
                    }
                  </Select>
                </FormControl>
              </Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Meter ID:</div>
              </Grid>

              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <TextField id="outlined-basic3" 
                  className={classes.TextField}
                  type="number"
                  onChange={
                    (e) => {
                      var temp = rs485[0]
                      temp.mst = e.target.value
                      temp.mst = Number(temp.mst)
                      setrs485([temp])
                      store.dispatch(RS485([temp]))
                    }
                  }
                  value={
                    ((rs485.length > 0) ? rs485[0].mst: "")
                  }
                  disabled={!(store.getState().RS485.length > 0)}
                  style={{width:"100%"}}
                  label="Meter ID" variant="outlined" />
              </Grid>


              <Grid  item lg={2} md={2} xl={2} xs={2}></Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Parity:</div>
              </Grid>

              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <FormControl variant="outlined" className={classes.formControl} style={{width:"100%"}} disabled={!(store.getState().RS485.length > 0)}>
                  <InputLabel htmlFor="outlined-age-native-simple">Parity</InputLabel>
                  <Select
                    native
                    label="Parity:"
                    inputProps={{
                      name: 'Parity:',
                      id: 'outlined-age-native-simple',
                    }}
                    onChange={(e) => {
                      setselect_model(e.target.value)
                      var temp = store.getState().RS485 //= e.target.value
                      var mpr = e.target.value
                      var mprout = 0
                      if (mpr === 'Even'){
                        mprout = 2
                      }else if (mpr === 'Old'){
                        mprout = 1
                      }else if (mpr === 'None'){
                        mprout = 0
                      }else {
                        mprout = 0
                      }
                      temp[0].mpr = Number(mprout)
                      store.dispatch(RS485(temp))
                    }}
                    value={select_parity}
                  >
                    {
                      serialparam.parity.map((item ,index) => {
                        return (
                          <option value={item} key={index}>{item}</option>
                        )
                      })
                    }
                  </Select>
                </FormControl>
              </Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Flow Total Register:</div>
              </Grid>

              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <TextField id="outlined-basic2" 
                  className={classes.TextField}
                  type="number"
                  style={{width:"100%"}}
                  onChange={
                    (e) => {
                      var temp = rs485[0]
                      temp.mrs[1].mba = e.target.value
                      temp.mrs[1].mba = Number(temp.mrs[1].mba)
                      setrs485([temp])
                      store.dispatch(RS485([temp]))
                    }
                  }
                  value={
                    // ((rs485.length > 0) ? rs485[0].mrs[1].mba: "")
                    ((rs485.length > 0) ? ((rs485[0].mrs.length > 0) ? rs485[0].mrs[1].mba : ""): "")
                  } 
                  label="Flow Total Register" variant="outlined" disabled={!(config.length > 0 && (store.getState().RS485[0].mml.length > 0) && (store.getState().RS485[0].mrs.length > 0))} />
              </Grid>
              
              <Grid  item lg={2} md={2} xl={2} xs={2}></Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Stop bits:</div>
              </Grid>

              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <FormControl variant="outlined" className={classes.formControl} style={{width:"100%"}} disabled={!(store.getState().RS485.length > 0)}>
                  <InputLabel htmlFor="outlined-age-native-simple">Stop bits</InputLabel>
                  <Select
                    native
                    label="Stop bits:"
                    inputProps={{
                      name: 'Stop bits:',
                      id: 'outlined-age-native-simple',
                    }}
                    onChange={(e) => {
                      setselect_model(e.target.value)
                      var temp = store.getState().RS485 //= e.target.value
                      temp[0].mst = Number(e.target.value)
                      store.dispatch(RS485(temp))
                    }}
                    value={select_stopbit}
                  >
                    {
                      serialparam.stopbit.map((item ,index) => {
                        return (
                          <option value={item} key={index}>{item}</option>
                        )
                      })
                    }
                  </Select>
                </FormControl>
              </Grid>

              <Grid  item lg={2} md={2} xl={2} xs={2} className={classes.rs485col1}>
                <div>Flow Rate Register:</div>
              </Grid>

              <Grid  item lg={3} md={3} xl={3} xs={3}>
                <TextField id="outlined-basic1" 
                  className={classes.TextField}
                  onChange={
                    (e) => {
                      var temp = rs485[0]
                      temp.mrs[0].mba = e.target.value
                      temp.mrs[0].mba = Number(temp.mrs[0].mba)
                      setrs485([temp])
                      store.dispatch(RS485([temp]))
                    }
                  }
                  value={
                    // ((rs485.length > 0) ? rs485[0].mrs[0].mba: "")
                    ((rs485.length > 0) ? ((rs485[0].mrs.length > 0) ? rs485[0].mrs[0].mba : ""): "")
                  } 
                  type="number"
                  style={{width:"100%"}}
                  label="Flow Rate Register" variant="outlined" disabled={!(config.length > 0 && (store.getState().RS485[0].mml.length > 0) && (store.getState().RS485[0].mrs.length > 0))} />

                  {/* (store.getState().RS485[0].mml.length > 0) && (store.getState().RS485[0].mrs.length > 0) */}


              </Grid>
              
              <Grid  item lg={2} md={2} xl={2} xs={2}></Grid>
            </Grid>
            
            <Grid container spacing={1} >
              <Button variant="contained" color="primary" style={{width: '100%'}}
                onClick={
                  prop.handleClickSaveRS485
                }
                disabled={!((store.getState().RS485.length > 0) ? ((store.getState().RS485[0].mml.length > 0) && (store.getState().RS485[0].mrs.length > 0) ? true: false): true)}
                // disabled={((rs458.length > 0) ? ((typeof rs458[0].mml === undefined) ? true : false) : false)}
              >
                Save Config to Logger
              </Button>
            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

RS485Config.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default RS485Config;