
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  TextField,
} from '@material-ui/core';
import { CONFIG } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "40px",
    fontSize: "20px",
  },
  DigitalInput: {
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#074c93",
    fontWeight: "700",
    marginLeft: "20px"
  },
  
}));

const DigitalInput = ({ className, store,  ...rest}) => {
  const classes = useStyles();
  const [config, setconfig] = React.useState([])

  React.useEffect(() => {
    const setcon = () => {
      setconfig(store.getState().Config)
    }
    setcon()
  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>Flowrate Averrage</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >
              {/* // Script */}
              <Grid container spacing={1} style = {{padding: "0px 0px 20px 0px"}}>
                <Grid item lg={2} md={2} xl={2} xs={12} className={classes.label}>
                  Flowrate Averrage Amount:
                </Grid>
                <Grid  item lg={3} md={3} xl={3} xs={3}>
                  <form className={classes.Inputbox + " " + classes.DigitalInput} noValidate autoComplete="off" >
                    <TextField id="outlined-basic4" 
                    onChange={
                      (e) => {
                        var temp = config[0]
                        temp.fam = Number(e.target.value)
                        setconfig([temp])
                        store.dispatch(CONFIG([temp]))
                      }
                    }
                    value={
                      ((config.length > 0) ? config[0].fam: "")
                    } 
                    type={'number'}
                    label="Amount us" variant="outlined" disabled={false} />
                  </form>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

DigitalInput.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default DigitalInput;