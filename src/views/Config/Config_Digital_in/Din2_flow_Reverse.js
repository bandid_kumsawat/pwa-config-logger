
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  TextField,
  Checkbox,
} from '@material-ui/core';
import { CONFIG } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'right',
    marginTop: "40px",
    fontSize: "20px",
  },
  labelCheckbox:{
    textAlign: 'left',
    marginTop: "30px",
    fontSize: "20px",
    display: "flex"
  },
  Din2_flow_Reverse: {
    textAlign: 'Left',
    marginTop: "20px",
    fontSize: "20px",
    color: "#074c93",
    fontWeight: "700",
    marginLeft: "20px"
  },
  checktitle:{
    marginTop: "10px"
  },
  titleLebel: {
    marginTop: "40px",
    width: "250px"
  },
  display_din1_flex: {
    display: "flex",
    fontSize: "20px",
    marginLeft: "30px",
  },
  display_btn_write: {
    marginTop:"10px",
    marginLeft: "50px"
  }
}));

const Din2_flow_Reverse = ({ className, store,  ...rest}) => {
  const classes = useStyles();
  const [config, setconfig] = React.useState([])
  const [checked, setChecked] = React.useState(true);

  React.useEffect(() => {
    const setcon = () => {
      setconfig(store.getState().Config)
      setChecked(((store.getState().Config[0].den & 2) ? true: false))
    }
    setcon()
  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>Din2_Flow Reverse</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >
              {/* // Script */}
              <Grid container spacing={1} style = {{padding: "0px 0px 20px 0px"}}>

                <Grid item lg={1} md={1} xl={1} xs={12} className={classes.labelCheckbox}>
                  <Checkbox
                  onClick={() => {
                    var status = !checked
                    var temp = config
                    if (status){
                      // +
                      temp[0].den = temp[0].den + 2
                    }else{
                      // -
                      temp[0].den = temp[0].den - 2
                    }
                    console.log(temp[0].den)
                    setChecked(status)
                    store.dispatch(CONFIG(temp))
                  }}
                  checked={checked}
                    style={{marginTop: '-10px'}}
                    color="primary"
                    inputProps={{ 'aria-label': 'all items selected' }}
                  />
                  <div className={classes.checktitle}>Enable</div>
                </Grid>
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.display_din1_flex}>
                  <div className={classes.titleLebel}>Liter Per Pulse:</div>
                  <form className={classes.Inputbox + " " + classes.Din2_flow_Reverse} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic4" 
                    onChange={
                      (e) => {
                        var temp = config[0]
                        temp.lp2 = Number(e.target.value)
                        setconfig([temp])
                        store.dispatch(CONFIG([temp]))
                      }
                    }
                    value={
                      ((config.length > 0) ? config[0].lp2: "")
                    } 
                    type={'number'}
                    style={{width: "100%"}}
                    label="Liter Per Pulse" variant="outlined" disabled={false} />
                  </form>
                </Grid>

                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.display_din1_flex}>
                  <div className={classes.titleLebel}>Flow Total:</div>
                  <form className={classes.Inputbox + " " + classes.Din2_flow_Reverse} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic3" 
                    onChange={
                      (e) => {
                        var temp = config[0]
                        temp.fa2 = Number(e.target.value)
                        setconfig([temp])
                        store.dispatch(CONFIG([temp]))
                      }
                    }
                    value={
                      ((config.length > 0) ? config[0].fa2: "")
                    } 
                    type={'number'}
                    label="Flow Total" variant="outlined" disabled={false} />
                  </form>
                </Grid>

                {/* <Grid item lg={2} md={2} xl={2} xs={12} className={classes.display_btn_write}>
                  <form className={classes.Inputbox + " " + classes.Din2_flow_Reverse} noValidate autoComplete="off">
                    <Button variant="contained" color="primary">
                      Write
                    </Button>
                  </form>
                </Grid> */}

              </Grid>
            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

Din2_flow_Reverse.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default Din2_flow_Reverse;