
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container, 
  Grid,
  FormControlLabel,
  Checkbox,
  Switch,
  TextField,
} from '@material-ui/core';

import { CONFIG } from 'src/AppStore/actions'

const useStyles = makeStyles((theme) => ({
  root: {},
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
}));

const AIN2 = ({ className, store,  ...props}) => {
  const classes = useStyles();
  const [ena] = React.useState(2)
  const [page] = React.useState('2')
  const [Title] = React.useState("Ain 2 Pressure 1(P_OUT)")
  const [config, setconfig] = React.useState(props.config)
  const [min, setmin] = React.useState(config[0]['mn' + page])
  const [max, setmax] = React.useState(config[0]['mx' + page])
  const [map_min, setmap_min] = React.useState(config[0]['mmn' + page])
  const [map_max, setmap_max] = React.useState(config[0]['pc' + page])
  const [factor, setfactor] = React.useState(config[0]['pt' + page])
  const [enable, setenable] = React.useState(((config[0].aen & ena) ? true: false)) 
  const [ischeck, setischeck] = React.useState(((config[0]['am' + page] === 1) ? true: false)) // 1 = 4-20a, 0 = 0-10v

  // const [min, setmin] = React.useState(config[0].mn1)
  // const [max, setmax] = React.useState(config[0].mx1)
  // const [map_min, setmap_min] = React.useState(config[0].mmn1)
  // const [map_max, setmap_max] = React.useState(config[0].pc1)
  // const [factor, setfactor] = React.useState(config[0].pt1)
  // const [enable, setenable] = React.useState(((config[0].aen & ena) ? true: false)) 
  // const [ischeck, setischeck] = React.useState(((config[0].am1 === 1) ? true: false)) // 1 = 4-20a, 0 = 0-10v

  React.useEffect(() => {

  })

  return (
    <div className={clsx(classes.root, className)}>
      <fieldset className={classes.FIELDSET}>
        <legend className={classes.LEGEND}>{Title}</legend>
        <div>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >
              
              {/* 1 */}
              <Grid container spacing={1} >
                <Grid item lg={3} md={3} xl={3} xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="checkedB"
                        color="primary"
                        checked={enable}
                        onChange={(e) => {
                          if (!enable){
                            config[0].aen+=ena
                          }else {
                            config[0].aen-=ena
                          }
                          setenable(!enable)
                          store.dispatch(CONFIG(config))
                        }}
                      />
                    }
                    label="Enable"
                  />
                </Grid>
                {/* <Grid item lg={3} md={3} xl={3} xs={12}>
                    <div style={
                      {
                        textAlign: "Right",
                        margin: "7px 0px 0px 0px"
                      }
                      }>
                      0-10 V
                    </div>
                </Grid> */}
                <Grid item lg={4} md={4} xl={4} xs={12}></Grid>
                <Grid item lg={5} md={5} xl={5} xs={12}>
                  <div style={{display:"flex", alignItems: "normal", justifyContent: "flex-end"}}>
                    <div style={{textAlign: "Right",margin: "7px 0px 0px 0px"}}>
                      0-10 V
                    </div>
                    <FormControlLabel
                      control={
                        <Switch
                          name="checkedB"
                          color="primary"
                          checked={ischeck}
                          onChange={(e) => {
                            config[0]["am"+page] = ((!ischeck) ? 1 : 0)
                            setischeck(!ischeck)
                            setconfig(config)
                            store.dispatch(CONFIG(config))

                            console.log(!ischeck)

                            if (!ischeck){
                              setmin(4)
                              setmax(20)
                              console.log([4, 20])
                              //min
                              config[0]['mn' + page] = Number(4)
                              setconfig(config)
                              store.dispatch(CONFIG(config))

                              //max
                              config[0]['mx' + page] = Number(20)
                              setconfig(config)
                              store.dispatch(CONFIG(config))
                            }else {
                              setmin(0)
                              setmax(10)
                              console.log([0, 10])
                              //min
                              config[0]['mn' + page] = Number(0)
                              setconfig(config)
                              store.dispatch(CONFIG(config))

                              //max
                              config[0]['mx' + page] = Number(10)
                              setconfig(config)
                              store.dispatch(CONFIG(config))
                            }


                          }}
                        />
                      }
                      style={{margin: "0px 0px 0px 13px"}}
                    />
                    <div style={{textAlign: "Right", margin: "7px 0px 0px 9px"}}>
                    4-20 mA
                    </div>
                  </div>
                </Grid>
                {/* <Grid item lg={3} md={3} xl={3} xs={12}>
                    <div style={
                      {
                        margin: "7px 0px 0px 0px"
                      }
                    }>
                      4-20 mA
                    </div>
                </Grid> */}
              </Grid>
              
              {/* 2 */}
              <Grid container spacing={1} >
                <Grid item lg={2} md={2} xl={2} xs={12}>
                  <div style={
                    {
                      textAlign: "Left",
                      margin: "16px 0px 0px 0px",
                      fontSize: "20px"
                    }
                    }>
                    min
                  </div>
                </Grid>
                <Grid item lg={5} md={5} xl={5} xs={12}>
                    <form className={classes.Inputbox} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic5" 
                    type={'number'}
                    style={{width: "100%", textAlign: "center"}}
                    value={min}
                    onChange={(e) => {
                      setmin(e.target.value)
                    }}
                    onBlur={() => {
                      config[0]['mn' + page] = Number(min)
                      setconfig(config)
                      store.dispatch(CONFIG(config))
                    }}
                    label="" variant="outlined" disabled={false} />
                  </form>
                </Grid>
              </Grid>

              {/* 3 */}
              <Grid container spacing={1} >
                <Grid item lg={2} md={2} xl={2} xs={12}>
                  <div style={
                    {
                      textAlign: "Left",
                      margin: "16px 0px 0px 0px",
                      fontSize: "20px"
                    }
                    }>
                    max
                  </div>
                </Grid>
                <Grid item lg={5} md={5} xl={5} xs={12}>
                  <form className={classes.Inputbox} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic5" 
                      type={'number'}
                      style={{width: "100%"}}
                      value={max}
                      onChange={(e) => {
                        setmax(e.target.value)
                      }}
                      onBlur={() => {
                        config[0]['mx' + page] = Number(max)
                        setconfig(config)
                        store.dispatch(CONFIG(config))
                      }}
                      label="" variant="outlined" disabled={false} />
                  </form>
                </Grid>
              </Grid>

              {/* 4 */}
              <Grid container spacing={1} >
                <Grid item lg={2} md={2} xl={2} xs={12}>
                  <div style={
                    {
                      textAlign: "Left",
                      margin: "16px 0px 0px 0px",
                      fontSize: "20px"
                    }
                    }>
                    map min
                  </div>
                </Grid>
                <Grid item lg={5} md={5} xl={5} xs={12}>
                  <form className={classes.Inputbox} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic5" 
                      type={'number'}
                      style={{width: "100%"}}
                      value={map_min}
                      onChange={(e) => {
                        setmap_min(e.target.value)
                      }}
                      onBlur={() => {
                        config[0]['mmn' + page] = Number(map_min)
                        setconfig(config)
                        store.dispatch(CONFIG(config))
                      }}
                      label="" variant="outlined" disabled={false} />
                  </form>
                </Grid>
                <Grid item lg={2} md={2} xl={2} xs={12}>
                    <div style={
                      {
                        textAlign: "Left",
                        margin: "16px 0px 0px 30px",
                        fontSize: "20px"
                      }
                    }>
                    bar
                  </div>
                </Grid>
              </Grid>

              {/* 5 */}
              <Grid container spacing={1} >
                <Grid item lg={2} md={2} xl={2} xs={12}>
                  <div style={
                    {
                      textAlign: "Left",
                      margin: "16px 0px 0px 0px",
                      fontSize: "20px"
                    }
                    }>
                    map max
                  </div>
                </Grid>
                <Grid item lg={5} md={5} xl={5} xs={12}>
                  <form className={classes.Inputbox} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic5" 
                      type={'number'}
                      value={map_max}
                      style={{width: "100%"}}
                      onChange={(e) => {
                        setmap_max(e.target.value)
                      }}
                      onBlur={() => {
                        config[0]['pc' + page] = Number(map_max)
                        setconfig(config)
                        store.dispatch(CONFIG(config))
                      }}
                      label="" variant="outlined" disabled={false} />
                  </form>
                </Grid>
                <Grid item lg={2} md={2} xl={2} xs={12}>
                    <div style={
                      {
                        textAlign: "Left",
                        margin: "16px 0px 0px 30px",
                        fontSize: "20px"
                      }
                    }>
                    bar
                  </div>
                </Grid>
              </Grid>


              {/* 6 */}
              <Grid container spacing={1} >
                <Grid item lg={2} md={2} xl={2} xs={12}>
                  <div style={
                    {
                      textAlign: "Left",
                      margin: "16px 0px 0px 0px",
                      fontSize: "20px"
                    }
                    }>
                    Factor
                  </div>
                </Grid>
                <Grid item lg={5} md={5} xl={5} xs={12}>
                  <form className={classes.Inputbox} noValidate autoComplete="off" style={{width: "100%"}}>
                    <TextField id="outlined-basic5" 
                      type={'number'}
                      style={{width: "100%"}}
                      value={factor}
                      onChange={(e) => {
                        setfactor(e.target.value)
                      }}
                      onBlur={() => {
                        config[0]['pt' + page] = Number(factor)
                        setconfig(config)
                        store.dispatch(CONFIG(config))
                      }}
                      label="" variant="outlined" disabled={false} />
                  </form>
                </Grid>
              </Grid>

            </Grid>
          </Container>
        </div>
      </fieldset>
    </div>
  );
};

AIN2.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default AIN2;