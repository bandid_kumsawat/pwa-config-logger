
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Card,
  CardContent,
  makeStyles,
  Button,
  AppBar,
  Tabs,
  Tab,
  Container,
  Grid,
  Dialog,
  TextField,
  DialogActions,
  DialogContent,
  CircularProgress,
  Snackbar,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { AlertTitle } from '@material-ui/lab';

import SettingsInputCompositeIcon from '@material-ui/icons/SettingsInputComposite';
import SettingsIcon from '@material-ui/icons/Settings';
import TuneIcon from '@material-ui/icons/Tune';
import WavesIcon from '@material-ui/icons/Waves';

import ViewDevice from './Config_Device/Device'
import ViewFrimware from './Config_Device/Frimware'
import ViewServer from './Config_Device/Server'
import ViweSim from './Config_Device/Sim'

import ViewFlowrateAverrage from './Config_Digital_in/Flowrate_Averrage'
import ViewDin1FlowForward from './Config_Digital_in/Din1_flow_Forward'
import ViewDin2FlowReverse from "./Config_Digital_in/Din2_flow_Reverse"
import ViewDin3FlowForward2 from "./Config_Digital_in/Din3_flow_Forward2"
import ViewDin4FlowReverse2 from "./Config_Digital_in/Din4_flow_Reverse2"
import ViewDin5 from "./Config_Digital_in/Din5"
import ViewDin6 from "./Config_Digital_in/Din6"

import ViewRS485Config from './Config_RS485/RS485Config'

import AIN1 from './Config_Analog_Precision/Ain_1'
import AIN2 from './Config_Analog_Precision/Ain_2'
import AIN3 from './Config_Analog_Precision/Ain_3'
import AIN4 from './Config_Analog_Precision/Ain_4'

import MQTT from 'paho-mqtt'
// import { store } from 'src/AppStore/store'
import { CONFIG, RS485, DEVICE, REFRESH, ISCONNECT } from 'src/AppStore/actions'
// import { result, update } from 'lodash';

const timeout = 10000
// Variable Time out
var check_time_out
var countmqsub = 0

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

// var client = new MQTT.Client('34.67.42.100', Number('8083'), "WEB_REACT_CONFIG_LOGGER");

const useStyles = makeStyles((theme) => ({
  root: {},
  rootTab: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: "#f6f6f6",
    marginTop: '0px',
  },
  ContainerButton: {
    padding: "10px 0px 10px 0px",
    display: "-webkit-box -moz-box -ms-flexbox -moz-flex -webkit-flex flex",
  },
  BTNChild: {
    width: '200px',
    height: "150px"
  },
  flewrow: {
    display: "flex",
    flexDirection: 'row'
  },
  textlabel: {
    width: '200px',
    textAlign: "center"
  },
  titleDialog:{
    fontSize: "24px",
    margin: "10px 0px 20px 20px"
  },
  Inputbox: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  label: {
    textAlign: 'Right',
    marginTop: "20px",
    fontSize: "20px"
  },
  progress: {
    display: 'flex'
  },
  progressText: {
    marginTop: '8px',
    marginLeft: "8px",
    mareginRight: "8px"
  },
  progress_waitupdate: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "center",
    marginTop: "20px"
  },
  progress_waitupdate_text: {
    marginLeft: "20px",
  },
  btnconfirm: {
    marginRight: "24px"
  }
}));

function TabPanelContrainer(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      //eslint-disable-next-line
      role="TabPanelContrainer"
      hidden={value !== index}
      id={`scrollable-force-TabPanelContrainer-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanelContrainer.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-TabPanelContrainer-${index}`,
  };
}

const ViewStock = ({ className, store,  ...props}) => {
  const classes = useStyles();

  const [client] = React.useState(props.client)
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(true);
  const [open_afterupdate, setOpen_afterupdate] = React.useState(false);
  const [Device, setDevice] = React.useState('')
  const [Config, setConifg] = React.useState([])
  const [Progress_status_save, setProgress_status_save] = React.useState(false)
  const [Progress_status_update, setProgress_status_update] = React.useState(false)
  const [Progress_status_update_rs485, setProgress_status_update_rs485] = React.useState(false)
  const [Have_data, setHave_data] = React.useState(false)
  const [Have_data_update, setHave_data_update] = React.useState(false)
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [vertical] = React.useState('bottom');
  const [horizontal] = React.useState('right');
  const [StatusUpdate, setStatusUpdate] = React.useState(true);
  const [isRS485update, setisRS485update] = React.useState(false);
  const [NotFound, setNotFound] = React.useState(true)
  const [openSnackbar_waitupdate, setopenSnackbar_waitupdate] = React.useState(false)

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleClose_afterupdate = () => {
    setOpen_afterupdate(false);
  };

  const handleInputDevice = (e) => {
    setDevice(e.target.value)
    store.dispatch(DEVICE(e.target.value))
  }

  client.onMessageArrived = (message) => {
    console.log(message)
    console.log(store.getState().isrefresh)
    var isreflash = store.getState().isrefresh
    var data = [JSON.parse(message.payloadString)]
    if (isreflash){
      console.log(countmqsub)
      if (data[0].ver === undefined) {
        store.dispatch(RS485(data))
        countmqsub++
      }else{
        store.dispatch(CONFIG(data))
        countmqsub++
      }
      if (countmqsub === 2){
        countmqsub = 0
        store.dispatch(REFRESH(false))
      }
    }else {
      console.log(data)
      if (data[0].ver === undefined) {
        store.dispatch(RS485(data))
        if (isRS485update){
          console.log('1')
          setProgress_status_update_rs485(false)
          setStatusUpdate(true)
          setopenSnackbar_waitupdate(true)
          setisRS485update(false)
          clearTimeout(check_time_out);
        }
      }else {
        store.dispatch(CONFIG(data))
        if (Have_data){
          console.log('2')
          clearTimeout(check_time_out);
          store.dispatch(CONFIG(data))
          setConifg(data)
          setOpen(false)
          setProgress_status_save(false)
          setNotFound(true)
          setHave_data(false)
          store.dispatch(ISCONNECT(true))
        }
      }
  
      // for update config
      if (Have_data_update){
        console.log('3')
        clearTimeout(check_time_out);
        setStatusUpdate(true)
        store.dispatch(CONFIG(data))
        setProgress_status_update(!Progress_status_update)
        setopenSnackbar_waitupdate(true)
        // setOpen_afterupdate(true)
      } 
    }
  }

  //save config
  const handleSaveConfig = () => {
    setHave_data_update(true)
    setProgress_status_update(!Progress_status_update)
    var message = new MQTT.Message(JSON.stringify(store.getState().Config[0])); // msg
    message.destinationName = "smartlogger/"+Device+"/device/config/set"
    client.send(message);
    setStatusUpdate(true)
    check_time_out = setTimeout(function(){ 
      console.log('fn_check_time_out')
      setStatusUpdate(false)
      setopenSnackbar_waitupdate(true)
      setProgress_status_update(false)
    }, timeout);
    // window.location.reload()
    // setTimeout(function(){ window.location.reload() }, 3000);
  }

  //save rs485
  const handleClickSaveRS485 = () => {
    setHave_data_update(false)
    var data = JSON.stringify(store.getState().RS485[0])
    console.log(data)
    var message = new MQTT.Message(data);
    message.destinationName = "smartlogger/"+Device+"/modbus/config/set"
    client.send(message);
    // window.location.reload()
    // setTimeout(function(){ window.location.reload() }, 3000);
    setProgress_status_update_rs485(true)
    setStatusUpdate(true)
    setisRS485update(true)
    check_time_out = setTimeout(function(){ 
      console.log('fn_check_time_out')
      setStatusUpdate(false)
      setopenSnackbar_waitupdate(true)
      setProgress_status_update_rs485(false)
    }, timeout);
  }

  const handlePublicConfig = () => {
    setHave_data(true)
    check_time_out = setTimeout(function(){ 
      console.log('fn_check_time_out')
      setHave_data(false)
      setProgress_status_save(false)
      if (!Have_data){
        setOpenSnackbar(true)
      }
      setNotFound(false)
    }, timeout);

    client.subscribe("smartlogger/"+Device+"/device/config");
    client.subscribe("smartlogger/"+Device+"/modbus/config");

    var message = new MQTT.Message("get config device"); // msg
    message.destinationName = "smartlogger/"+Device+"/device/config/get"; // topic
    client.send(message);

    message = new MQTT.Message("get config rs485"); // msg
    message.destinationName = "smartlogger/"+Device+"/modbus/config/get"; // topic
    client.send(message);

    setProgress_status_save(true)
  }

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };
  const handleCloseSnackbar_waitupdate = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setopenSnackbar_waitupdate(false);
  };
  React.useEffect(() => {

  })

  return (
    <div>
      <Snackbar open={openSnackbar} anchorOrigin={{ vertical, horizontal }} autoHideDuration={10000} onClose={handleCloseSnackbar}>
        <Alert onClose={handleCloseSnackbar} severity="error">
          ไม่พบอุปกรณ์
        </Alert>
      </Snackbar>

      <Snackbar open={openSnackbar_waitupdate} anchorOrigin={{ vertical, horizontal }} autoHideDuration={5000} onClose={handleCloseSnackbar_waitupdate}>
        <Alert onClose={handleCloseSnackbar_waitupdate} severity={((StatusUpdate) ? "success": "error")}>
          {
            ((StatusUpdate) ? "อัพเดทคอนฟิกเสร็จเรียบร้อย" : "ไม่สามารถอัพเดทคอนฟิกได้")
          }
        </Alert>
      </Snackbar>

      <Dialog
        open={open_afterupdate}
        onClose={handleClose_afterupdate}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth={'md'}
        fullWidth={true}
      >
        <DialogContent>
          <MuiAlert severity="error" style={{width:"100%"}}>
            <AlertTitle>Error</AlertTitle>
              ไม่สามารถอัพเดทคอนฟิกได้
          </MuiAlert>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose_afterupdate} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose_afterupdate} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        disableBackdropClick
        fullWidth={true}
        maxWidth={'sm'}
        open={open}
        disableEscapeKeyDown={true}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <div className={classes.titleDialog}>กรอก Serial No. ของ Logger</div>
        <DialogContent>
          <Container maxWidth={false}>
            <Grid
              container
              spacing={1}
            >

              {/* // APN:  */}
              <Grid container spacing={1} >
                <Grid item lg={4} md={4} xl={4} xs={12} className={classes.label}>
                  Device Name: 
                </Grid>
                <Grid  item lg={8} md={8} xl={8} xs={8} >
                  <form className={classes.Inputbox} autoComplete="off">
                    <TextField 
                      error={!NotFound}
                      id="outlined-basic1" 
                      label="Device Name" 
                      variant="outlined" 
                      value={Device} 
                      helperText={((!NotFound) ? "ไม่พบอุปกรณ์": "")}
                      onChange={handleInputDevice}/>
                  </form>
                </Grid>
              </Grid>

            </Grid>
          </Container>
        </DialogContent>
        <DialogActions style ={{marginBottom: "10px", marginRight: "-10px"}}>
          {
            ((Progress_status_save) ? <div className={classes.progress}>
            <CircularProgress /> 
            <div className={classes.progressText}>กรุณารอซักครู่ . . . </div>
          </div> : "")
          }
          <Button className={classes.btnconfirm} onClick={handlePublicConfig} variant="contained" color="primary" style = {{fontSize: "18px"}} disabled={Progress_status_save || (Device.length === 0)}>
            ยืนยัน
          </Button>
        </DialogActions>
      </Dialog>
      <Card
        className={clsx(classes.root, className)}
        {...props}
      >
        <CardContent>
          <div className={classes.flewrow}>
            {/* Read Config to Logger */}

            {/* <div className={classes.ContainerButton}>
              <div className={classes.flewcolumn}>
                <Button className={classes.BTNChild} variant="contained" color="primary" disabled={false} onClick={
                  () => {
                    window.location.reload()
                  }
                }>
                  <InputIcon style = {{width: "150px", height: "100px"}}/>
                </Button>
              </div>
              <div className={classes.textlabel}>Read Config Logger</div>
            </div> */}

            {/* Save Config to Logger */}
            {/* <div className={classes.ContainerButton} style={{marginLeft: "50px"}}>
              <div className={classes.flewcolumn}>
                <Button className={classes.BTNChild} variant="contained" color="primary" 
                onClick={
                  handleSaveConfig
                }
                disabled={!(store.getState().Config.length)} >
                  <SaveIcon style = {{width: "150px", height: "100px"}}/>
                </Button>
              </div>
              <div className={classes.textlabel}>Save Config Logger</div>
            </div> */}
            
            {/* Read File RS485.json */}
            {/* <div className={classes.ContainerButton} style={{marginLeft: "50px"}}>
              <div className={classes.flewcolumn}>
                <Button className={classes.BTNChild} variant="contained" color="primary" disabled={true}>
                  <img alt={'icon-read'} src={'/static/icon-read.svg'}></img>
                </Button>
              </div>
              <div className={classes.textlabel}>Read File RS485.json</div>
            </div> */}

            {/* Write File RS485.json */}
            {/* <div className={classes.ContainerButton} style={{marginLeft: "50px"}}>
              <div className={classes.flewcolumn}>
                <Button className={classes.BTNChild} variant="contained" color="primary" disabled={true}>
                  <img alt={'icon-read'} src={'/static/icon-read.svg'}></img>
                </Button>
              </div>
              <div className={classes.textlabel}>Write File RS485.json</div>
            </div> */}

          </div>


          <div className={classes.rootTab}>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs example"
              >
                <Tab label="Device" icon={<SettingsIcon />} {...a11yProps(0)} disabled ={false} style ={{fontSize: "20px"}}/>
                <Tab label="Digital_I" icon={<SettingsInputCompositeIcon />} {...a11yProps(1)} disabled ={false} style ={{fontSize: "20px"}}/>
                <Tab label="Analog_I" icon={<WavesIcon />} {...a11yProps(3)} disabled ={false} style ={{fontSize: "20px"}}/>
                <Tab label="RS485" icon={<TuneIcon />} {...a11yProps(4)} disabled ={false} style ={{fontSize: "20px"}}/>
              </Tabs>
            </AppBar>
            <TabPanelContrainer value={value} index={0}>
              <Container maxWidth={false}>
                <Grid
                  container
                  spacing={1}
                >
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={12}
                  >
                    <Grid
                      container
                      spacing={1}
                    >
                      {/* Column 1 */}
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDevice config={Config} store={store}/>
                      </Grid>
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewServer config={Config} store={store} />
                      </Grid>
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViweSim config={Config} store={store} />
                      </Grid>
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewFrimware config={Config} store={store}/>
                      </Grid>
                    </Grid>
                  </Grid>

                  {/* Column 2 */}
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={12}
                  >
                    <Grid
                      container
                      spacing={1}
                    >
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        {/* <ViewTime config={Config} store={store} hidden={true}/> */}
                      </Grid>
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        {/* <ViewSDCard config={Config} store={store} hidden={true}/> */}
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid
                  container
                  spacing={1}
                >
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={6}
                  >
                    <Button variant="contained" color="primary" 
                    onClick={
                      handleSaveConfig
                    }
                    style = {{width: "100%"}}>Save Config to Logger</Button>
                  </Grid>
                </Grid>

                {/* show status mqtt wait update */}
                <Grid
                  container
                  spacing={1}
                >
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={6}
                  >
                    {
                      ((Progress_status_update) ? <div className={classes.progress_waitupdate}>
                      <CircularProgress  /><div className={classes.progress_waitupdate_text}>กรุณารอซักครู่</div>
                    </div> : 
                    "")
                    }
                  </Grid>
                </Grid>


              </Container>
            
            </TabPanelContrainer>
            <TabPanelContrainer value={value} index={1}>
              <Container maxWidth={false}>
                <Grid
                  container
                  spacing={1}
                >
                  <Grid
                    item
                    lg={12}
                    md={12}
                    xl={12}
                    xs={12}
                  >
                    <Grid
                      container
                      spacing={1}
                    >
                      {/* Column 1 */}
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewFlowrateAverrage config={Config} store={store}/>
                      </Grid>

                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDin1FlowForward config={Config} store={store}/>
                      </Grid>

                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDin2FlowReverse config={Config} store={store}/>
                      </Grid>

                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDin3FlowForward2 config={Config} store={store}/>
                      </Grid>

                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDin4FlowReverse2 config={Config} store={store}/>
                      </Grid>

                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDin5 config={Config} store={store}/>
                      </Grid>

                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewDin6 config={Config} store={store}/>
                      </Grid>
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                         <Button variant="contained" color="primary" style={{width: "100%"}} 
                          onClick={
                            handleSaveConfig
                          }
                         >
                            Save Config to Logger
                          </Button>
                      </Grid>
                      {/* show status mqtt wait update */}
                      <Grid
                        container
                        spacing={1}
                      >
                        <Grid
                          item
                          lg={12}
                          md={12}
                          xl={12}
                          xs={12}
                        >
                          {
                            ((Progress_status_update) ? <div className={classes.progress_waitupdate}>
                            <CircularProgress  /><div className={classes.progress_waitupdate_text}>กรุณารอซักครู่</div>
                          </div> : 
                          "")
                          }
                        </Grid>
                      </Grid>
                      
                    </Grid>
                  </Grid>
                </Grid>
              </Container>
            </TabPanelContrainer>
            <TabPanelContrainer value={value} index={2}>
              <Container maxWidth={false}>
                <Grid container spacing={1}>
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={6}
                  >
                    <AIN1 config={Config} store={store}/>
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={6}
                  >
                    <AIN3 config={Config} store={store} />
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={6}
                  >
                    <AIN2 config={Config} store={store} />
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    md={6}
                    xl={6}
                    xs={6}
                  >
                    <AIN4 config={Config} store={store} />
                  </Grid>
                </Grid>
                <Grid container spacing={1}>
                  <Grid
                    item
                    lg={12}
                    md={12}
                    xl={12}
                    xs={12}
                  >
                    <Button variant="contained" color="primary" 
                    onClick={
                      handleSaveConfig
                    }
                    style = {{width: "100%"}}>Save Config to Logger</Button>
                  </Grid>
                </Grid>

                <Grid container spacing={1}>
                  <Grid
                    item
                    lg={12}
                    md={12}
                    xl={12}
                    xs={12}
                  >
                    {
                      ((Progress_status_update) ? <div className={classes.progress_waitupdate}>
                      <CircularProgress  /><div className={classes.progress_waitupdate_text}>กรุณารอซักครู่</div>
                    </div> : 
                    "")
                    }
                  </Grid>
                </Grid>

              </Container>
            </TabPanelContrainer>
            <TabPanelContrainer value={value} index={3}>
            <Container maxWidth={false}>
                <Grid
                  container
                  spacing={1}
                >
                  <Grid
                    item
                    lg={12}
                    md={12}
                    xl={12}
                    xs={12}
                  >
                    <Grid
                      container
                      spacing={1}
                    >
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        <ViewRS485Config handleClickSaveRS485={handleClickSaveRS485}client={client} config={Config} store={store}/>
                      </Grid>
                      {/* <Button variant="contained" color="primary" style={{width: '100%'}}
                        onClick={
                          handleClickSaveRS485
                        }
                        disabled={!((store.getState().RS485.length > 0) ? ((store.getState().RS485[0].mml.length > 0) && (store.getState().RS485[0].mrs.length > 0) ? true: false): true)}
                        // disabled={((rs458.length > 0) ? ((typeof rs458[0].mml === undefined) ? true : false) : false)}
                      >
                        Save Config RS485 to Logger
                      </Button> */}
                    </Grid>
                    <Grid
                      container
                      spacing={1}
                    >
                      <Grid
                        item
                        lg={12}
                        md={12}
                        xl={12}
                        xs={12}
                      >
                        {
                          ((Progress_status_update_rs485) ? <div className={classes.progress_waitupdate}>
                          <CircularProgress  /><div className={classes.progress_waitupdate_text}>กรุณารอซักครู่</div>
                        </div> : 
                        "")
                        }
                      </Grid>
                    </Grid>

                  </Grid>
                </Grid>
              </Container>
            </TabPanelContrainer>
          </div>

        </CardContent>
        {/* <Divider />  */}
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
        </Box>
      </Card>
    </div>
  );
};

ViewStock.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default ViewStock;


// - Topic Detail
//   1.  get config    = smartlogger/SM03-002/config/get
//       response      = smartlogger/SM03-002/config
//   2.  update config = smartlogger/SM03-002/config/set
//       response      = smartlogger/SM03-002/confg
//   3.  get RS485     = smartlogger/SM03-002/modbus/config/get
//       response      = smartlogger/SM03-002/modbus/config
//   4.  update RS485  = smartlogger/SM03-002/modbus/config/set
//       response      = smartlogger/SM03-002/modbus/config



// web config    = http://34.67.42.100/
// node-read     = http://34.67.42.100:1880
//   user: admin
//   pass: deaw1234
//   สำหรับเทส
// EMQ dashboard = http://34.67.42.100:18083
//   user: admin
//   pass: public