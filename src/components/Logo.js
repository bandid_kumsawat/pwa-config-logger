import React from 'react';

const Logo = (props) => {
  return (
    <img
      alt="Logo"
      src="/static/logo-pwa.svg"
      {...props}
      width="50px"
      height="50px"
    />
  );
};

export default Logo;
