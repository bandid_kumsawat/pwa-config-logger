import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import MainLayout from 'src/layouts/MainLayout';
import Config from 'src/views/Config';
import MQTT from 'paho-mqtt'

// client.connect(
//   {cleanSession : false,
//       onSuccess : onConnectSuccess,
//       onFailure : onFailedConnect,
//       keepAliveInterval : 10,
//       reconnect : true});
var client
try{
  
  client = new MQTT.Client('164.115.22.31', Number('8083'), "__LOGGER_WEB_CONFIG_" + Math.random().toString().slice(2,10));
  client.onConnectionLost = (responseObject) => {
    if (responseObject.errorCode !== 0) {
      console.log('onConnectionLost')
      console.log(responseObject.errorMessage);
      // window.location.reload();
      // alert("ไม่สามารถเชื่อมต่อกับ Broker ได้")
      window.location.reload();
    }
  };

  client.connect({onSuccess:onConnect, keepAliveInterval : 10, reconnect : true});

  function onConnect() {
    console.log("onConnect");
  }

}catch(e){
  
  console.log(e)

}

const routes = [
  {
    path: 'app',
    element: <DashboardLayout client={client}/>,
    children: [
      { path: 'Config', element: <Config client={client}/> },
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: '/', element: <Navigate to="/app/Config" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
